package exercise;

public class Exercise05 {
    public static int chuSoKhacKhong(int n) {
        int res = 1;
        for (int i = 2; i <= n; i++) {
            res *= i;
            while (res % 10 == 0) {
                res = res / 10;
            }
        }
        while(res % 10 == 0) {
            res /= 10;}

        int giaithua = res % 10;
        return giaithua;
    }


    public static void main(String[] args) {
        int ketqua = chuSoKhacKhong(5);
        System.out.println(ketqua);
    }
}
