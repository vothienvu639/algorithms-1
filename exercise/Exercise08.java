package exercise;

public class Exercise08 {
    public static int primeSum(int n) {
        int res = 0;
        for (int i = 2; i <= n; i++){
            boolean iIsPrime = true;
            for(int j = 2; j <= Math.sqrt(i); j++){
                if (i == 2) {
                    break;
                }
                if(i % j == 0){
                    iIsPrime = false;
                }
            }

            if(iIsPrime) {
                res += i % 22082018;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(primeSum(5));
    }
}
