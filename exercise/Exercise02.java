package exercise;

public class Exercise02 {
    static int solve(int n){
        int k = 2, sum = 0;
        while (n > 1){
            while (n % k == 0){
                sum += k;
                n /= k;
            }
            k++;
        }
        return sum;
    }
    static int factorSum(int n)
    {
        while (n != solve(n))
            n = solve(n);
        return n;
    }

    public static void main(String[] args) {
        System.out.println(factorSum(24));
    }
}
