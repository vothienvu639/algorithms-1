package exercise;

public class Exercise06 {
    public static int uocSo(int n){
        if (n == 1){
            return 1;
        }
        if (n == 0){
            return 10;
        }
        int ans = 0;
        for(int i = 9; i >= 2; i--) {
            while (n % i == 0) {
                ans = ans * 10 + i;
                n /= i;
            }
        }
        int ans2 = 0;
        while(ans > 0){
            ans2 = ans2 * 10 + ans % 10;
            ans /= 10;
        }
        return (n == 1) ? ans2 : -1;
    }

    public static void main(String[] args) {
        int res = uocSo(450);
        System.out.println(res);
    }

}
