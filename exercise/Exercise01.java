package exercise;

public class Exercise01 {
    public static void main(String[] args) {
        boolean res = soNguyenTo(11);
        System.out.println(res);
    }

   public static boolean soNguyenTo(int n){
        if (n <= 1){
            return false;
        }
        for (int i = 2; i < n; i++){
            if (n % 2 ==0){
                return false;
            }
        }
        return true;
   }
}
