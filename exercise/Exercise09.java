package exercise;

public class Exercise09 {
    public static long numberZeroDigits(long n) {
        long d = 0;
        long k = 5;
        while (k <= n){
            d += n / k;
            k *= 5;
        }
        return d;
    }

    public static void main(String[] args) {
        System.out.println(numberZeroDigits(5));
    }
}
