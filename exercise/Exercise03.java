package exercise;

import java.util.ArrayList;
import java.util.List;

public class Exercise03 {
    static List<Integer> data = new ArrayList<>();

    static void isDivisor(int n) {
        for(int i = 2; i <= n - 1; i++) {
            if(n % i == 0) {
                data.add(i);
            }
        }
        data.add(1);
        data.add(n);
    }

    static int isPrime(int n) {
        if(n == 2) {
            return n;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if(n % i == 0) {
                return -1;
            }
        }
        return n;
    }

    static int greatestCommonPrimeDivisor(int a, int b) {
        isDivisor(Math.min(a, b));
        int maxPrime = -1;
        for(Integer x: data) {
            int n = isPrime(x);
            if (n > maxPrime) {
                maxPrime = n;
            }
        }
        return maxPrime;
    }

    public static void main(String[] args) {
        System.out.println(greatestCommonPrimeDivisor(2, 3));
    }
}
