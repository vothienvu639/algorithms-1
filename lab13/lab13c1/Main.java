package lab13.lab13c1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static List<Integer> answer (int []a, int[]b){
        List<Integer> output = new ArrayList<>();
        for (int i = 0; i < b.length; i++){
            int temp = -1;
            for (int j = 0; j <a.length; j++){
                if (b[i] == a[j]){
                    temp = j;
                }
            }
            output.add(temp);
        }
        return output;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int []a = new int[n];
        for (int i = 0; i < n; i++){
            a[i] = sc.nextInt();
        }
        int k = sc.nextInt();
        int []b = new int[k];
        for (int j = 0; j < k; j++){
            b[j] = sc.nextInt();
        }
        System.out.println(answer(a, b));

    }
}
