package lab12.lab12c4;
import java.util.*;

public class CoveringSegments {
    private static List<Integer> optimalPoints(Segment[] segments) {
        //write your code here
        List<Integer> points = new ArrayList<>();
//        int milestone = (int) Math.round(segments.length/2.0);
        int milestone = segments.length;
        boolean isDeletedArr = false;
        do {
            isDeletedArr = false;
            one: for(int i = 0; i < segments.length; i++) {
                int count = 0;
                Set<Integer> segmentIndexWillRemove = new HashSet<>();
                for(int j = 0; j < segments.length; j++){
                    if(segments[j].start <= segments[i].end && segments[i].end <= segments[j].end) {
                        count++;
                        segmentIndexWillRemove.add(j);
                    }
                }
                if(count >= milestone) {
                    points.add(segments[i].end);
                    if(i != segments.length - 1 ) {
                        List<Segment> segmentsNew = new ArrayList<>(Arrays.asList(segments));
                        for(int removeIndex: segmentIndexWillRemove) {
                            segmentsNew.remove(removeIndex);
                        }
                        segments = segmentsNew.toArray(new Segment[0]);
                        isDeletedArr = true;
                        break one;
                    }
                } else if (i == segments.length - 1) {
                    for(int h = 0; h < segments.length; h++) {
                        points.add(segments[i].end);
                    }
                }
            }

        } while (isDeletedArr);
        return points;
    }

    private static class Segment {
        int start, end;

        Segment(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Segment[] segments = new Segment[n];
        for (int i = 0; i < n; i++) {
            int start, end;
            start = scanner.nextInt();
            end = scanner.nextInt();
            segments[i] = new Segment(start, end);
        }
        List<Integer> points = optimalPoints(segments);
        System.out.println(points.size());
        for (int point : points) {
            System.out.print(point + " ");
        }
    }
}
