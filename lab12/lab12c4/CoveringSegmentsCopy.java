package lab12.lab12c4;
import java.util.*;

public class CoveringSegmentsCopy {
    private static List<Integer> optimalPoints(Segment[] segments) {
        //write your code here
        List<Integer> points = new ArrayList<>();
        Map<Integer, Set<Integer>> pointNeeds = new HashMap<>();
        for (Segment segment : segments) {
            Set<Integer> segmentIndexNeed = new HashSet<>();
            for (int j = 0; j < segments.length; j++) {
                if (segments[j].start <= segment.end && segment.end <= segments[j].end) {
                    segmentIndexNeed.add(j);
                }
            }
            pointNeeds.put(segment.end, segmentIndexNeed);
        }
        Set<Integer> selectedIndex = new HashSet<>();
        do {
            Iterator<Integer> keySet = pointNeeds.keySet().iterator();
            int maxOption = -1;
            while (keySet.hasNext()) {
                int key = keySet.next();
                Set<Integer> value = pointNeeds.get(key);
                value.removeIf(selectedIndex::contains);
                pointNeeds.put(key, value);
                if(maxOption == -1) {
                    maxOption = key;
                } else if(pointNeeds.get(key).size() > pointNeeds.get(maxOption).size()) {
                    maxOption = key;
                }
            }
            points.add(maxOption);
            selectedIndex.addAll(pointNeeds.get(maxOption));
            pointNeeds.remove(maxOption);
        } while (selectedIndex.size() < segments.length);
        return points;
    }

    private static class Segment {
        int start, end;

        Segment(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Segment[] segments = new Segment[n];
        for (int i = 0; i < n; i++) {
            int start, end;
            start = scanner.nextInt();
            end = scanner.nextInt();
            segments[i] = new Segment(start, end);
        }
        List<Integer> points = optimalPoints(segments);
        System.out.println(points.size());
        for (int point : points) {
            System.out.print(point + " ");
        }
    }
}
