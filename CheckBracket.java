import java.util.Scanner;
import java.util.Stack;

public class CheckBracket {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String text = sc.nextLine();
        Stack<Bracket> openBS = new Stack<>();
        int position;
        for(position = 0; position < text.length(); position++){
            char next = text.charAt(position);
            if(next == '{' || next == '[' || next == '('){
                Bracket bracket = new Bracket(next, position+1);
                openBS.push(bracket);
            }else if(next == '}' || next == ']' || next == ')'){
                Bracket previousB = openBS.peek();
                if(previousB.Math(next)){
                    openBS.pop();
                }else {
                    position++;
                    break;
                }
            }
        }
        if(openBS.empty()){
            System.out.println("success");
        }else{
            System.out.println(position);
        }
    }
}
