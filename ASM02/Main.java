package ASM02;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    /**
     * hàm hiển thị menu chức năng
     */
    public static void menu() {
        System.out.println("+--------Menu--------+");
        System.out.println("| 1. Manual Input    |");
        System.out.println("| 2. File Input      |");
        System.out.println("| 3. Bubble sort     |");
        System.out.println("| 4. Selection sort  |");
        System.out.println("| 5. Insertion sort  |");
        System.out.println("| 6. Search > value  |");
        System.out.println("| 7. Search = value  |");
        System.out.println("| 0. Exit            |");
        System.out.println("+--------------------+");
    }

    /**
     * hàm nhập input
     */
    public static void writeInputFile() {
        Scanner sc = new Scanner(System.in);
//        nhập độ dài của chuỗi số
        System.out.println("Please enter input number of elements: ");
        int n = sc.nextInt();
//        nhập từng số
        System.out.println("Please enter input elements: ");
        StringBuilder stringBuilder = new StringBuilder();
        while (n > 0) {
            stringBuilder.append(sc.nextDouble());
            stringBuilder.append("\n");
            n--;
        }
        writeDataToFile(stringBuilder.toString().strip());
        System.out.println("Nhap so thanh cong!");

    }

    /**
     * hàm viết data vào file
     * @param data
     */
    private static void writeDataToFile(String data) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the file path: ");
        String filePath = sc.next();
        try {
            FileWriter fWirter = new FileWriter(filePath);
            fWirter.write(data);
            fWirter.close();
        } catch (IOException e) {
            System.out.println("Created file is fail.");
            System.out.println(e.getMessage());
        }
    }


    /**
     * hàm đọc input
     * @param filePath
     * @return doubles
     */
    public static List<Double> readInputFile(String filePath) {
        List<Double> doubles = new ArrayList<>();
        try {
            File file = new File(filePath);
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                doubles.add(sc.nextDouble());
            }
            System.out.print("Input array: ");
            for (double number : doubles) {
                System.out.print(number + " ");
            }
            System.out.println();

        } catch (FileNotFoundException e) {
            System.out.println("File is not found");
        }
        return doubles;
    }


    /**
     * hàm sắp xếp theo bubbleSort
     * @param filePath
     */
    public static void bubbleSort(String filePath) {
        List<Double> doubles = readInputFile(filePath);
        double[] arr = doubles.stream().mapToDouble(Double::doubleValue).toArray();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    double temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
            String str = Arrays.toString(arr);
            str = str.replaceAll("\\[|\\]|,", " ");
            System.out.println(str);
        }
        writeDataToFile(Arrays.toString(arr));
    }

    /**
     * hàm sắp xếp theo selectionSort
     * @param filePath
     */
    public static void selectionSort(String filePath) {
        List<Double> doubles = readInputFile(filePath);
        double[] arr = doubles.stream().mapToDouble(Double::doubleValue).toArray();
        for (int i = 0; i < arr.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[min]) {
                    min = j;
                }
            }
            double temp = arr[min];
            arr[min] = arr[i];
            arr[i] = temp;
            String str = Arrays.toString(arr);
            str = str.replaceAll("\\[|\\]|,", " ");
            System.out.println(str);
        }
        writeDataToFile(Arrays.toString(arr));

    }

    /**
     * hàm sắp xếp theo insertSort
     * @param filePath
     */
    public static void insertSort(String filePath) {
        List<Double> doubles = readInputFile(filePath);
        double[] arr = doubles.stream().mapToDouble(Double::doubleValue).toArray();
        for (int i = 1; i < arr.length; i++) {
            double key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
            String str = Arrays.toString(arr);
            str = str.replaceAll("\\[|\\]|,", " ");
            System.out.println(str);
        }
        writeDataToFile(Arrays.toString(arr));


    }


    /**
     * hàm tìm kiếm tuần tự
     * @param filePath
     */
    public static void searchValue(String filePath) {
        List<Double> doubles = readInputFile(filePath);
        double[] arr = doubles.stream().mapToDouble(Double::doubleValue).toArray();
        StringBuilder stringBuilder = new StringBuilder();
        Scanner sc = new Scanner(System.in);
//        nhap gia tri can tim kiem
        System.out.println("Please enter searched input value: ");
        double value = sc.nextDouble();
        System.out.print("Larger position: ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > value) {
                System.out.print(i + " ");
                stringBuilder.append(i);
                stringBuilder.append("\n");
            }
        }
        System.out.println();
        writeDataToFile(stringBuilder.toString().strip());

    }


    /**
     * hàm tìm kiếm nhị phân
     * @param filePath
     */
    public static void binarySearch(String filePath) {
        List<Double> doubles = readInputFile(filePath);
        double[] arr = doubles.stream().mapToDouble(Double::doubleValue).toArray();
        Scanner sc = new Scanner(System.in);
//        nhap gia tri can tim kiem
        System.out.println("Please enter searched input value: ");
        double value = sc.nextDouble();
        int l = 0, r = arr.length - 1;
        System.out.print("The right position: ");
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (arr[m] == value) {
                System.out.println(m);
                break;
            }
            if (arr[m] < value)
                l = m + 1;
            else
                r = m - 1;
        }
    }

    /**
     * hàm nhập số chức năng
     * @return số chức năng (number)
     */
    public static int inputModuleNumber() {
        boolean flag = false;
        int number = 0;
        do {
            System.out.print("Chon chuc nang: ");
            try {
                Scanner sc = new Scanner(System.in);
                number = sc.nextInt();
                flag = false;
            } catch (Exception ex) {
                flag = true;
                System.out.println("Vui long nhap so cua chuc nang");
            }
        } while (flag);
        return number;
    }


    public static void main(String[] args) {
        while (true) {
            menu();
            int chucNang = 0;
            Scanner sc = new Scanner(System.in);
            do {
                chucNang = inputModuleNumber();
                switch (chucNang) {
                    case 1:
                        writeInputFile();
                        break;
                    case 2:
                        String filePath = sc.next();
                        readInputFile(filePath);
                        break;
                    case 3:
//                        nhập đường dẫn file
                        System.out.println("Please enter the file path:");
                        String filePath1 = sc.next();
                        bubbleSort(filePath1);
                        break;
                    case 4:
//                        nhập đường dẫn file
                        System.out.println("Please enter the file path:");
                        String filePath2 = sc.next();
                        selectionSort(filePath2);
                        break;
                    case 5:
//                        nhập đường dẫn file
                        System.out.println("Please enter the file path:");
                        String filePath3 = sc.next();
                        insertSort(filePath3);
                        break;
                    case 6:
//                        nhập đường dẫn file
                        System.out.println("Please enter the file path:");
                        String filePath4 = sc.next();
                        searchValue(filePath4);
                        break;
                    case 7:
//                        nhập đường dẫn file
                        System.out.println("Please enter the file path:");
                        String filePath5 = sc.next();
                        binarySearch(filePath5);
                        break;
                    case 0:
                        System.out.println("Thanks!!!");
                        System.exit(0);
                    default:
                        System.out.println("Chon lai chuc nang");
                }
            } while (true);
        }
    }
    }

