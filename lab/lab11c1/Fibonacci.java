package lab.lab11c1;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Scanner;

public class Fibonacci {
    private static long calc_fib(int n) {
        if (n <= 1)
            return n;

        return calc_fib(n - 1) + calc_fib(n - 2);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        LocalDateTime startTime = LocalDateTime.now();
        System.out.println(calc_fib(n));
        System.out.println(Duration.between(startTime, LocalDateTime.now()).toSeconds());
    }
}