import java.util.Scanner;

public class bai16c2c1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhap n: ");
        int n = scanner.nextInt();
        Node head = null;
        Node node = null;
        for(int i = 0; i < n; i++) {
            System.out.print("Nhap phan tu thu " + (i + 1) +": ");
            if (i == 0) {
                node = new Node(scanner.nextInt());
                head = node;
            } else {
                node.next = new Node(scanner.nextInt());
                node = node.next;
            }
        }

        // chen du lieu vao vi tri dau tien
        // vi du co 1 day so 4 6 7 1 8
        // head = 4
        // h muon chen so 5 vao vi tri dau tien
        Node temp = head;
        head = new Node(5);
        head.next = temp;

        // In du lieu
        System.out.print("In data: ");
        while (head != null) {
            System.out.print(head.data + " ");
            head = head.next;
        }

        // xoa du lieu o vi tri dau tien
        // vi du co 1 day so 5 4 6 7 1 8
        // head = 5
        // h muon xoa so 5 o vi tri dau tien
        head = head.next;


        // chen du lieu vao vi tri cuoi
        // vi du co 1 day so 4 6 7 1 8
        // head = 4
        // h muon chen so 5 vao vi tri cuoi
        if(head == null) {
            head = new Node(5);
        } else {
            Node last = head;
            while (last.next != null) {
                last = last.next;
            }
            last.next = new Node(5);
        }


        // In du lieu
        System.out.print("In data: ");
        while (head != null) {
            System.out.print(head.data + " ");
            head = head.next;
        }

    }

    static class Node {
        int data;
        Node next;
        Node(int d) {
            data = d;
            next = null;
        }
    }
}
