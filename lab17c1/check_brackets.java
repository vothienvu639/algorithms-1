package lab17c1;

import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

class Bracket {
    Bracket(char type, int position) {
        this.type = type;
        this.position = position;
    }

    boolean Match(char c) {
        if (this.type == '[' && c == ']')
            return true;
        if (this.type == '{' && c == '}')
            return true;
        if (this.type == '(' && c == ')')
            return true;
        return false;
    }

    char type;
    int position;
}

public class check_brackets {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        Stack<Bracket> opening_brackets_stack = new Stack<Bracket>();
        int position;
        for (position = 0; position < text.length(); ++position) {
            char next = text.charAt(position);

            if (next == '(' || next == '[' || next == '{') {
                // Process opening bracket, write your code here
                Bracket bracket = new Bracket(next, position + 1);
                opening_brackets_stack.push(bracket);
            }

            else if (next == ')' || next == ']' || next == '}') {
                // Process closing bracket, write your code here
                Bracket previousBracket = opening_brackets_stack.peek();
                if (previousBracket.Match(next)) {
                    opening_brackets_stack.pop();
                } else {
                    position+=1;
                    break;
                }

            }
        }

        // Printing answer, write your code here
        if (opening_brackets_stack.empty()) {
            System.out.println("Success");
        } else {
            System.out.println(position);
        }
    }
}
