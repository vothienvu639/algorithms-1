import java.util.Scanner;

public class main {
    public static class Node{
        int data;
        Node next;
        Node(int d){
            data = d;
            next = null;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("nhap n: ");
        int n = sc.nextInt();
        Node head = null;
        Node node = null;
        for(int i=0; i < n; i++){
            System.out.println("nhap phan tu thu " + (i +1) + " ");
            if(i ==0){
                node = new Node(sc.nextInt());
                head = node;
            }else {
                node.next = new Node(sc.nextInt());
                node = node.next;
            }

        }
//        chen vao
//            Node temp = head;
//            head = new Node(5);
//            head.next = temp;

//        xoa
//        head = head.next;

//        chen vij tri cuoi cung
        if(head == null){
            head = new Node(5);
        }else{
            Node last = head;
            while(last.next != null){
                last = last.next;
            }
            last.next = new Node(5);
        }




        System.out.println("In data ");
        while(head != null){
            System.out.println(head.data + " ");
            head = head.next;
        }
    }

}
