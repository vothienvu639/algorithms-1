public class Bracket {
    char text;
    int position;

     Bracket(char text, int position) {
        this.text = text;
        this.position = position;
    }
    boolean Math(char c){
         if(this.text == '{' && c == '}')
             return true;
         if(this.text == '[' && c == ']')
             return true;
         if(this.text == '(' && c == ')')
             return true;
        return false;
    }
}
