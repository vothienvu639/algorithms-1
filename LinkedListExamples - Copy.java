public class LinkedListExamples {
    Node head;  // đầu danh sách

    static class Node {
        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }

    /* Hàm này in ra nội dung của danh sách liên kết bắt đầu từ head */
    public void display() {
        Node n = head;
        while (n != null) {
            System.out.print(n.data + " \n");
            n = n.next;
        }
    }

    /* phương thức tạo một danh sách liên kết đơn giản với 3 nút*/
    public static void main(String[] args) {
        /* Bắt đầu với danh sách rỗng */
        LinkedListExamples list = new LinkedListExamples();

        list.head = new Node(100);
        Node second = new Node(200);
        Node third = new Node(300);

        list.head.next = second; // Liên kết nút đầu với nút thứ hai
        second.next = third; // Liên kết nút thứ hai với nút thứ ba
        list.display();
    }
}  